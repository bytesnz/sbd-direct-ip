#!/bin/env node

import { createServer } from '../index.js';
import minimist from 'minimist';

const options = minimist(process.argv.slice(2), {
	alias: {
		a: 'allowlist',
		b: 'blocklist'
	}
});

if (typeof options.allowlist === 'string') {
	options.allowlist = [options.allowlist];
}

if (typeof options.denylist === 'string') {
	options.denylist = [options.denylist];
}

const server = await createServer(options);

server.listen();
